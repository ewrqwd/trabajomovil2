package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.media.Image;
import android.os.Trace;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    TextView Precio, Nombre,Descripcion;
    ImageView Img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6

            String object = getIntent().getStringExtra("object_id");
            Precio = (TextView) findViewById(R.id.precio);
            Nombre = (TextView) findViewById(R.id.nombre);
            Descripcion = (TextView) findViewById(R.id.descripcion);
            Img = (ImageView) findViewById(R.id.imagen);

            //accede
            DataQuery query = DataQuery.get("item");
            String parametro = getIntent().getExtras().getString("objeto1");
            // recibe parametro
        query.getInBackground(parametro, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e==null) {
                    //recibe las propiedades
                    String Precio1 = (String) object.get("price");
                    String Descripcion1 = (String) object.get("descripcion");
                    String Nombre1 = (String) object.get("name");
                    Bitmap ImagenBitmap = (Bitmap) object.get("image");

                    // guarda los datos
                    Precio.setText(Precio1);
                    Descripcion.setText(Descripcion1);
                    Nombre.setText(Nombre1);
                    Img.setImageBitmap(ImagenBitmap);



                } else {}

                    //error
                    //fin - code



            }});

                }
            }

